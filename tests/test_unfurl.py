#!/usr/bin/python

# Imports
from furl import furl
from unfurl import unfurl

# Tests
def test_unfurl():
    url = 'http://user:pass@example.com/path/to/something'
    furled = furl(url)
    unfurled = unfurl(furled)
    assert url == unfurled

