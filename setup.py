from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


config = {
  'name' : 'unfurl',
     'version' : '0.1',
     'description' : 'Unfurls a furled URL.',
     'long_description' : readme(),
     'classifiers' : [
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7',
      ],
     'keywords' : 'URL furl joke',
     #'url' : 'URL',
     'author' : 'Christopher Wrinn',
     'author_email' : 'cwrinn@paladinlogic.com',
     'license' : 'MIT',
     'packages' : ['unfurl'],
     'install_requires' : [
         'furl',
      ],
     'setup_requires' : [
         'pytest-runner',
      ],
     'tests_require' : [
         'pytest',
      ],
     'include_package_data' : True,
     'zip_safe' : False
}

setup(**config)
