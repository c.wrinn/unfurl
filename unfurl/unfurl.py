#!/usr/bin/python

# Imports
from furl import furl

def unfurl(furled):
    return furled.url


if __name__ == '__main__':
  raise NotImplementedError("This is not a console script.")
